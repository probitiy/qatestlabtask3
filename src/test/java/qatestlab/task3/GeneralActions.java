package qatestlab.task3;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import qatestlab.task3.models.ProductData;
import qatestlab.task3.pages.*;

/**
 * Contains main script actions that may be used in scripts.
 */
public class GeneralActions {
    private EventFiringWebDriver driver;
    private WebDriverWait wait;
    private By contentElement = By.className("breadcrumb");

    //    Часть А. Создание продукта:
//            1. Войти в Админ Панель.
//            2. Выбрать пункт меню Каталог -> повары и дождаться загрузки страницы продуктов.
//            3. Нажать «Новый товар» для перехода к созданию нового продукта, дождаться загрузки
//    страницы.
//            4. Заполнить следующие свойства нового продукта: Название продукта, Количество, Цена.
//    Свойства продукта должны генерироваться случайно (случайное название продукта,
//                                                      количество от 1 до 100, цена от 0.1 ₴ до 100 ₴).
//            5. После заполнения полей активировать продукт используя переключатель на нижней
//    плавающей панели. После активации продукта дождаться всплывающего уведомления
//    о сохранении настроек и закрыть его.
//            6. Сохранить продукт нажав на кнопку «Сохранить». Дождаться всплывающего
//    уведомления о сохранении настроек и закрыть его.
//    Часть Б. Проверка отображения продукта:
//            1. Перейти на главную страницу магазина.
//            2. Перейти к просмотру всех продуктов воспользовавшись ссылкой «Все товары».
//    Добавить проверку (Assertion), что созданный в Админ Панели продукт отображается на
//    странице.
//3. Открыть продукт. Добавить проверки, что название продукта, цена и количество
//    соответствует значениям, которые вводились при создании продукта в первой части
//    сценария.

    public GeneralActions(EventFiringWebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    /**
     * Logs in to Admin Panel.
     * @param login
     * @param password
     */
    public void login(String login, String password) {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.open();
        loginPage.fillEmailInput(login);
        loginPage.fillPasswordInput(password);
        loginPage.clickSubmitButton();
    }

    public void createProduct(ProductData newProduct) {
        DashboardPage dashboardPage = new DashboardPage(driver);
        dashboardPage.hoverCatalog();
        dashboardPage.clickProductsLink();
        ProductsPage productsPage = new ProductsPage(driver);
        productsPage.clickCreateProductButton();

        NewProductPage newProductPage = new NewProductPage(driver, newProduct);
        newProductPage.fillProductName();
        newProductPage.fillProductPrice();
        newProductPage.fillProductQuantity();
        newProductPage.activateProduct();
        newProductPage.clickSaveProductButton();
        newProductPage.checkAndCloseNotice();
    }

    public void checkProductVisibility(ProductData newProduct) {
        ShopMainPage shopMainPage = new ShopMainPage(driver);
        shopMainPage.open();
        shopMainPage.clickAllProductsLink();

        ShopProductsPage shopProductsPage = new ShopProductsPage(driver);
        shopProductsPage.searchProduct(newProduct.getName());

        ShopSearchResultPage shopSearchResultPage = new ShopSearchResultPage(driver);
        shopSearchResultPage.checkElementPresence(newProduct);
        shopSearchResultPage.openElement(newProduct);

        ProductShowPage productShowPage = new ProductShowPage(driver);
        productShowPage.checkProductValues(newProduct);
    }

    /**
     * Waits until page loader disappears from the page
     */
    public void waitForContentLoad() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(contentElement));
    }
}