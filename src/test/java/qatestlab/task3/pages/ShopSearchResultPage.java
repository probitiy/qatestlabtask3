package qatestlab.task3.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import qatestlab.task3.models.ProductData;

import java.util.List;

/**
 * Created by alexander on 01/12/2017.
 */
public class ShopSearchResultPage {
    private WebDriverWait wait;
    private EventFiringWebDriver driver;
    private By productSearchInput = By.className("ui-autocomplete-input");
    private By productSearchButton = By.cssSelector("#search_widget button[type=submit]");


    public ShopSearchResultPage(EventFiringWebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);
    }

    public void checkElementPresence(ProductData newProduct) {
        List<WebElement> foundElements = driver.findElements(By.cssSelector(".product-description .h3.product-title"));
        Assert.assertTrue(findElementWithByName(foundElements, newProduct.getName()) != null);
    }

    public void openElement(ProductData newProduct) {
        findElementWithByName(driver.findElements(By.cssSelector(".product-description .h3.product-title")), newProduct.getName()).click();
    }

    private WebElement findElementWithByName(List<WebElement> elements, String name) {
        for(WebElement element: elements) {
            if (element.getText().equals(name)) {
                return element;
            }
        }
        return null;
    }
}
