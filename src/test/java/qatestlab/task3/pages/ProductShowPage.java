package qatestlab.task3.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import qatestlab.task3.models.ProductData;

/**
 * Created by alexander on 01/12/2017.
 */
public class ProductShowPage {
    private EventFiringWebDriver driver;
    private WebDriverWait wait;
    private By productPrice = By.cssSelector(".product-prices .product-price .current-price");
    private By productQuantity = By.cssSelector(".product-quantities span");
    private By productName = By.cssSelector("h1[itemprop=name]");

    public ProductShowPage(EventFiringWebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);
    }

    public void checkProductValues(ProductData newProduct) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(productPrice));
        Assert.assertEquals(newProduct.getName().toUpperCase(), driver.findElement(productName).getText(), "Name is not equals");
        Assert.assertEquals(newProduct.getPrice(), driver.findElement(productPrice).getText().split(" ")[0], "Price is not equals");
        Assert.assertEquals(newProduct.getQty().toString(), driver.findElement(productQuantity).getText().split(" ")[0], "Quantity is not equals");
    }
}
