package qatestlab.task3.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by alexander on 01/12/2017.
 */
public class DashboardPage {
    private EventFiringWebDriver driver;
    private WebDriverWait wait;
    private By catalogLink = By.id("subtab-AdminCatalog");
    private By productsLink = By.id("subtab-AdminProducts");
    private By ajaxLoading = By.id("ajax_running");

    public DashboardPage(EventFiringWebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);
    }

    public void hoverCatalog() {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(ajaxLoading));
        wait.until(ExpectedConditions.visibilityOfElementLocated(catalogLink));
        Actions action = new Actions(driver);
        action.moveToElement(driver.findElement(catalogLink)).build().perform();
    }

    public void clickProductsLink() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(productsLink));
        driver.findElement(productsLink).click();
    }
}
