package qatestlab.task3.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by alexander on 01/12/2017.
 */
public class ShopMainPage {
    private String mainPageUrl = "http://prestashop-automation.qatestlab.com.ua/ru/";
    private EventFiringWebDriver driver;
    private WebDriverWait wait;
    private By allProductsLink = By.className("all-product-link");

    public ShopMainPage(EventFiringWebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);
    }

    public void open() {
        driver.get(mainPageUrl);
    }

    public void clickAllProductsLink() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(allProductsLink));
        driver.findElement(allProductsLink).click();
    }
}
