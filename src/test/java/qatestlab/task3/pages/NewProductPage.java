package qatestlab.task3.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import qatestlab.task3.models.ProductData;

/**
 * Created by alexander on 01/12/2017.
 */
public class NewProductPage {
    private EventFiringWebDriver driver;
    private WebDriverWait wait;
    private By productNameInput = By.id("form_step1_name_1");
    private By productQuantityInput = By.id("form_step1_qty_0_shortcut");
    private By productPriceInput = By.id("form_step1_price_shortcut");
    private By productSaveButton = By.cssSelector(".btn.btn-primary.js-btn-save");
    private By productActiveSwitch = By.className("switch-input");
    private By successNotice = By.cssSelector("#growls .growl-notice");
    private By noticeCloseLink = By.className("growl-close");
    private ProductData newProduct;

    public NewProductPage(EventFiringWebDriver driver, ProductData newProduct) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);
        this.newProduct = newProduct;
    }


    public void fillProductName() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(productNameInput));
        driver.findElement(productNameInput).sendKeys(newProduct.getName());
    }

    public void fillProductQuantity() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(productQuantityInput));
        driver.findElement(productQuantityInput).clear();
        driver.findElement(productQuantityInput).sendKeys(newProduct.getQty().toString());
    }

    public void fillProductPrice() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(productPriceInput));
        driver.findElement(productPriceInput).clear();
        driver.findElement(productPriceInput).sendKeys(newProduct.getPrice());
    }

    public void activateProduct() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(productActiveSwitch));
        driver.findElement(productActiveSwitch).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(successNotice));
    }

    public void clickSaveProductButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(productSaveButton));
        driver.findElement(productSaveButton).click();
    }

    public void checkAndCloseNotice() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(successNotice));
        driver.findElement(successNotice).findElement(noticeCloseLink).click();
    }
}
