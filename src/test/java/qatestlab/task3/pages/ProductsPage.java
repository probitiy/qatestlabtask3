package qatestlab.task3.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by alexander on 01/12/2017.
 */
public class ProductsPage {
    private EventFiringWebDriver driver;
    private WebDriverWait wait;
    private By createProductButton = By.id("page-header-desc-configuration-add");

    public ProductsPage(EventFiringWebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);
    }

    public void clickCreateProductButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(createProductButton));
        driver.findElement(createProductButton).click();
    }


}
