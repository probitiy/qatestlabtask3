package qatestlab.task3.utils.logging;

/**
 * Created by alexander on 30/11/2017.
 */
        import org.openqa.selenium.By;
        import org.openqa.selenium.WebDriver;
        import org.openqa.selenium.WebElement;
        import org.openqa.selenium.support.events.WebDriverEventListener;

        import java.util.Arrays;


public class EventHandler implements WebDriverEventListener {

    @Override
    public void beforeAlertAccept(WebDriver webDriver) {

    }

    @Override
    public void afterAlertAccept(WebDriver webDriver) {

    }

    @Override
    public void afterAlertDismiss(WebDriver webDriver) {

    }

    @Override
    public void beforeAlertDismiss(WebDriver webDriver) {

    }

    public void beforeNavigateTo(String url, WebDriver driver) {
        CustomReporter.log("Navigate to " + url);
    }

    public void afterNavigateTo(String url, WebDriver driver) {

    }

    public void beforeNavigateBack(WebDriver driver) {
        CustomReporter.log("Navigate back");
    }

    public void afterNavigateBack(WebDriver driver) {
        CustomReporter.log("Current URL: " + driver.getCurrentUrl());
    }

    public void beforeNavigateForward(WebDriver driver) {
        CustomReporter.log("Navigate forward");
    }

    public void afterNavigateForward(WebDriver driver) {
        CustomReporter.log("Current URL: " + driver.getCurrentUrl());
    }

    public void beforeNavigateRefresh(WebDriver driver) {
        CustomReporter.log("Refresh page");
    }

    public void afterNavigateRefresh(WebDriver driver) {
        CustomReporter.log("Current URL: " + driver.getCurrentUrl());
    }

    public void beforeFindBy(By by, WebElement element, WebDriver driver) {
        CustomReporter.log("Search for element " + by.toString());
    }

    public void afterFindBy(By by, WebElement element, WebDriver driver) {
        if (element != null) {
            CustomReporter.log("Found element " + element.getTagName());
        }
    }

    public void beforeClickOn(WebElement element, WebDriver driver) {
        CustomReporter.log("Click on element " + element.getTagName());
    }

    public void afterClickOn(WebElement element, WebDriver driver) {
    }

    public void beforeChangeValueOf(WebElement element, WebDriver driver, CharSequence[] keysToSend) {
//        String value = Arrays.stream(keysToSend).map(CharSequence::toString).collect(Collectors.joining());
//        CustomReporter.log(String.format("Change value of %s: %s\n", element.getTagName(), value));
    }

    public void afterChangeValueOf(WebElement element, WebDriver driver, CharSequence[] keysToSend) {
        CustomReporter.log(String.format("Changed element " + element.getTagName()));
    }

    public void beforeScript(String script, WebDriver driver) {
        CustomReporter.log("Execute script: " + script);
    }

    public void afterScript(String script, WebDriver driver) {
        CustomReporter.log("Script executed");
    }

    public void onException(Throwable throwable, WebDriver driver) {
        // already logged by reporter
    }
}