package qatestlab.task3.tests;

/**
 * Created by alexander on 30/11/2017.
 */
        import org.testng.annotations.*;
        import qatestlab.task3.BaseTest;
        import qatestlab.task3.models.ProductData;

public class CreateProductTest extends BaseTest {

    @Test(dataProvider="LoginCredentials")
    @Parameters({"login", "password"})
    public void createNewProduct(String login, String password) {
        ProductData newProduct = ProductData.generate();
        actions.login(login, password);
        log("Login finished");
        actions.createProduct(newProduct);
        log("Product created");
        actions.checkProductVisibility(newProduct);
        log("Product checks passed");
    }
}