package qatestlab.task3;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Reporter;
import qatestlab.task3.utils.logging.EventHandler;
import org.testng.annotations.*;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by alexander on 30/11/2017.
 */
public abstract class BaseTest {
    protected EventFiringWebDriver driver;
    protected GeneralActions actions;

    private WebDriver getDriver(String browser) {
        if (browser.equals("firefox")) {
            System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "/drivers/geckodriver");
            return new FirefoxDriver();
        } else if (browser.equals("safari")) {
            return new SafariDriver();
        } else {
            System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/drivers/chromedriver");
            return new ChromeDriver();
        }
    }


    @BeforeClass
    @Parameters("browser")
    public void setUp(String browser ) {
        driver = new EventFiringWebDriver(getDriver(browser));
        driver.register(new EventHandler());

        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        Reporter.setEscapeHtml(false);



        actions = new GeneralActions(driver);
    }

    @DataProvider(name="LoginCredentials")
    public Object[][] getDataFromDataprovider(){
        return new Object[][]
                {
                        { "webinar.test@gmail.com", "Xcg7299bnSmMuRLp9ITw" }
                };

    }

    @AfterClass
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }

    protected void log(String message) {
        Reporter.log(message + "<br/>");
    }
}
